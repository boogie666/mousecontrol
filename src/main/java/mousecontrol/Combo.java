package mousecontrol;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Combo implements ICombo{

    private final List<Integer> keyPresses;
    private final List<Integer> keyReleases;
    private final Robot robot;
    public Combo(Robot robot, int ... keys) {
        this.robot = robot;
        keyPresses = new ArrayList<>(keys.length);
        keyReleases = new ArrayList<>(keys.length);

        for(int key : keys) {
            keyPresses.add(key);
            keyReleases.add(0, key);
        }

    }

    @Override
    public void execute() {
        this.keyPresses.forEach(robot::keyPress);
        this.keyReleases.forEach(robot::keyRelease);
    }
}
