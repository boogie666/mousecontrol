package mousecontrol;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class MainThreadEventDispatcher extends AbstractExecutorService {

    private volatile boolean running;

    public MainThreadEventDispatcher() {

    }

    @Override
    public void shutdown() {
        this.running = false;
    }

    @Override
    public List<Runnable> shutdownNow() {
        this.running = false;
        return new ArrayList<>(0);
    }

    @Override
    public boolean isShutdown() {
        return !running;
    }

    @Override
    public boolean isTerminated() {
        return !running;
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) {
        return true;
    }

    public void process() {
        this.running = true;
        //block forever
        while (running) {}
    }

    @Override
    public void execute(Runnable command) {
        command.run();
    }
}
