package mousecontrol;

import org.jnativehook.NativeHookException;
import org.jnativehook.mouse.NativeMouseEvent;

import java.awt.event.KeyEvent;

public class Main {

    public static void main(String[] args) throws NativeHookException {

        final MouseControl mouseControl = new MouseControl();

        mouseControl.addCombo(NativeMouseEvent.BUTTON4, KeyEvent.VK_CONTROL, KeyEvent.VK_RIGHT);
        mouseControl.addCombo(NativeMouseEvent.BUTTON5, KeyEvent.VK_CONTROL, KeyEvent.VK_LEFT);

        mouseControl.start();
    }

}
