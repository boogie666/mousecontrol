package mousecontrol;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.NativeInputEvent;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputAdapter;

import java.awt.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MouseControl {

    private final Map<Integer, ICombo> combos;
    private final Robot robot;
    
    public MouseControl() {
        combos = new HashMap<>();
        try {
            this.robot = new Robot();
        } catch (AWTException e) {
            throw new RuntimeException(e);
        }
    }

    public MouseControl addCombo(int mouseButton, int...keys){
        this.combos.put(mouseButton, new Combo(this.robot, keys));
        return this;
    }


    public void start() throws NativeHookException {
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        logger.setUseParentHandlers(true);

        final MainThreadEventDispatcher processor = new MainThreadEventDispatcher();
        GlobalScreen.registerNativeHook();
        GlobalScreen.setEventDispatcher(processor);
        GlobalScreen.addNativeMouseListener(new NativeMouseInputAdapter(){
            @Override
            public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) {
                if(nativeMouseEvent.getClickCount() == 1) {
                    ICombo combo = combos.get(nativeMouseEvent.getButton());
                    if(combo != null){
                        combo.execute();
                        consumeEvent(nativeMouseEvent);
                    }
                }
            }

        });

        processor.process();

        GlobalScreen.unregisterNativeHook();
    }


    private static void consumeEvent(NativeMouseEvent event){
        try {
            Field f = NativeInputEvent.class.getDeclaredField("reserved");
            f.setAccessible(true);
            f.setShort(event, (short) 0x01);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
