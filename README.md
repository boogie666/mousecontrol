# Mouse Control

A tiny tool that lets you program mouse buttons to emit key combos.


# Building 

`mvn package` will create the .app file.

The `Mouse Control.app` will be located in `target/mousecontrol-1.0-SNAPSHOT/Mouse Control.app`



# Setup

The app need access to accesibity control in order to emit virtual key combos.

Go to `System Preferences -> Security & Privacy -> Privacy -> Accessibility`
Add the `Mouse Control.app` application to the list of allowed apps.

Open a terminal and type `open "Mouse Control.app"` from the apps folder.
This will launch the application for the first time. (the terminal does not need to stay open after this point)

Currently the only combos that are pre-programmed are
`CTRL + Left Arrow` bound to mouse button 5 and `CTRL + Right Arrow` bound to mouse button 4

This will swipe your screen to the left and right respectively.

# Start at login

Go to `System Preferences -> Users & Groups -> "your user" -> Login Items` and add the application to the list



# Adding aditional combos

Edit `src/main/java/mousecontrol/Main.java` file to add aditional mouse combos.
